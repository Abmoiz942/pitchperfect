//
//  playviewUIcontrollerViewController.swift
//  PitchPerfect
//
//  Created by Jawad Ali on 12/22/19.
//  Copyright © 2019 xyz. All rights reserved.
//

import UIKit
import AVFoundation

class playviewUIcontrollerViewController: UIViewController {
    //var recordedAudioUrl : URL!
    @IBOutlet weak var snailButton: UIButton!
    @IBOutlet weak var chipmunkButton: UIButton!
    @IBOutlet weak var rabbitButton: UIButton!
    @IBOutlet weak var vaderButton: UIButton!
    @IBOutlet weak var echoButton: UIButton!
    @IBOutlet weak var reverbButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    var recordedAudioURL:URL!
    var audioFile:AVAudioFile!
    var audioEngine:AVAudioEngine!
    var audioPlayerNode: AVAudioPlayerNode!
    var stopTimer: Timer!
    
    
    

    enum ButtonType: Int {
        case slow = 0, fast, chipmunk, vader, echo, reverb
    }

    override func viewDidLoad() {
        super.viewDidLoad()
       setupAudio()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureUI(.notPlaying)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    
    
    @IBAction func playSoundForButton(_ sender: UIButton) {
        print("Play Sound Button Pressed")
        switch (ButtonType(rawValue:sender.tag)!) {
       case .slow:
            playSound(rate: 0.5)
            ToastUI.shared.short(self.view, txt_msg: "playing Slow")
        case .fast:
            playSound(rate: 1.5)
            ToastUI.shared.short(self.view, txt_msg: "playing fast")
        case .chipmunk:
            playSound(pitch: 1000)
            ToastUI.shared.short(self.view, txt_msg: "playing low pitch")
        case .vader:
            playSound(pitch: -1000)
            ToastUI.shared.short(self.view, txt_msg: "playing high pitch")
        case .echo:
            playSound(echo: true)
            ToastUI.shared.short(self.view, txt_msg: "playing Echo")
        case .reverb:
            playSound(reverb: true)
            ToastUI.shared.short(self.view, txt_msg: "playing reverb")
            }
        
       configureUI(.playing)
        }

    @IBAction func stopButtonPressed(_ sender: AnyObject) {
        print("Stop Audio Button Pressed")
        stopAudio()
        ToastUI.shared.short(self.view, txt_msg: "Stop Playing...")
    }

}
