//
//  ViewController.swift
//  PitchPerfect
//
//  Created by Jawad Ali on 11/24/19.
//  Copyright © 2019 xyz. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, AVAudioRecorderDelegate{

    @IBOutlet weak var recordingstatus: UILabel!
    @IBOutlet weak var recordingbutton: UIButton!
    @IBOutlet weak var stoprecordingbutton: UIButton!
    var audioRecorder :AVAudioRecorder!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        stoprecordingbutton.isEnabled=false
        recordingbutton.isEnabled=true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear Called...")
    }

    @IBAction func stoprecording(_ sender: Any) {
               print("RECORDING STOPS.....")
               recordingstatus.text="Tap to Recording"
               stoprecordingbutton.isEnabled=false
               recordingbutton.isEnabled=true
        
        audioRecorder.stop()
        let audiosession = AVAudioSession.sharedInstance()
        try! audiosession.setActive(false)
        
    }
    
    @IBAction func checkmethod(_ sender: Any) {
         print("RECORDING STARTS.....")
        
        ToastUI.shared.short(self.view, txt_msg: "Recording start")
         recordingstatus.text="Recording in Progress.."
         stoprecordingbutton.isEnabled=true
         recordingbutton.isEnabled=false
        
        
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask, true)[0] as String
        let recordingName = "recordedVoice.wav"
        let pathArray = [dirPath, recordingName]
        let filePath = URL(string: pathArray.joined(separator: "/"))
        

        let session = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)

        try! audioRecorder = AVAudioRecorder(url: filePath!, settings: [:])
        audioRecorder.delegate=self
        audioRecorder.isMeteringEnabled = true
        audioRecorder.prepareToRecord()
        audioRecorder.record()
        }
    

    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        print("Recording Finsihed....")
        if flag{
        performSegue(withIdentifier: "stopRecording", sender: audioRecorder.url)
        }else{
            print("Recording is not sucessfully send..")
        }
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "stopRecording"{
        let playsoundVC = segue.destination as! playviewUIcontrollerViewController
        let recordedAudioUrl = sender as! URL
            playsoundVC.recordedAudioURL = recordedAudioUrl
            }
        
        
    }
    
    
    
    
    
}

